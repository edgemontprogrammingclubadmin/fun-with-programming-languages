Program PascalsTriangle;

Const
	height = 25;

Var 
	i : integer;
	j : integer;
	triangle: array[-1..height + 1, -1..height + 1] of int64;
Begin
	triangle[0, 0] := 1;

	For i:=1 to height do
	Begin
		For j:= 0 to i do
		Begin
			triangle[i, j] := triangle[i - 1, j] + triangle[i, j] + triangle[i - 1, j - 1];
		End;
	End;

	For i := 0 to height do
	Begin
		For j := 0 to i do
		Begin
			Write(triangle[i, j]);
			Write(' ');
		End; 
		Writeln();
	End
End.